//
//  main.m
//  JenkinsTest
//
//  Created by Neerajpal on 26/04/16.
//  Copyright © 2016 Neerajpal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
