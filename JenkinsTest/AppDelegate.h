//
//  AppDelegate.h
//  JenkinsTest
//
//  Created by Neerajpal on 26/04/16.
//  Copyright © 2016 Neerajpal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

